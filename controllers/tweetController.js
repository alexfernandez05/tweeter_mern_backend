import Tweet from '../models/Tweet.js';
import Usuario from '../models/Usuario.js';

const obtenerTweets = async (req, res) => {
    const tweets = await Tweet.find().populate('usuario').populate('likes');

    res.json(tweets);
};

const obtenerTweet = async (req, res) => {
    const { id } = req.params;
    const tweet = await Tweet.findById(id).populate('usuario');

    res.json(tweet);
};

const nuevoTweet = async (req, res) => {
    const tweet = new Tweet(req.body);
    tweet.usuario = req.usuario;

    try {
        const tweetAlmacenado = await tweet.save();
        res.json(tweetAlmacenado);
    } catch (error) {
        console.log(error);
    }
};

const editarTweet = async (req, res) => {
    const { id } = req.params;
    const tweet = await Tweet.findById(id).populate('usuario');

    if (!tweet) {
        const error = new Error('Tweet no encontrado');
        res.status(404).json({ msg: error.message });
    };

    const tweetFromRequest = req.body.tweet;

    tweet.esRespuesta = tweetFromRequest.esRespuesta || tweet.esRespuesta;
    tweet.usuario = tweetFromRequest.usuario || tweet.usuario;
    tweet.fechaPublicacion = tweetFromRequest.fechaPublicacion || tweet.fechaPublicacion;
    tweet.contenido = tweetFromRequest.contenido || tweet.contenido;
    tweet.imagen = tweetFromRequest.imagen || tweet.imagen;
    tweet.comentarios = tweetFromRequest.comentarios || tweet.comentarios;
    tweet.retweets = tweetFromRequest.retweets || tweet.retweets;
    tweet.likes = tweetFromRequest.likes || tweet.likes;
    tweet.guardados = tweetFromRequest.guardados || tweet.guardados;
    tweet.listaComentarios = tweetFromRequest.listaComentarios || tweet.listaComentarios;

    try {
        const tweetAlmaceando = await tweet.save();
        res.json(tweetAlmaceando);
    } catch (error) {
        console.log(error);
    }
};

export {
    obtenerTweets,
    obtenerTweet,
    nuevoTweet,
    editarTweet,
};