import nodemailer from 'nodemailer';

export const emailRegistro = async (datos) => {

    const { email, nombre, token } = datos;

    const transport = nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        secure: true,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS,
        },
    })

    // Información del email
    const info = await transport.sendMail({
        from: '"Tweeter - Descubre lo que esta pasando" <cuentas@tweeter.com>',
        to: email,
        subject: 'Tweeter - Confirmación de cuenta',
        text: 'Comprueba tu cuenta en Tweeter',
        html: `
        <p>Hola: ${nombre} Comprueba tu cuenta en Tweeter</p>
        <p>Tu cuenta ya esta casi lista, solo debes comprobarla en el siguinete enlace:</p>
        <a href="${process.env.FRONTEND_URL}/confirmar/${token}">Comprobar Cuenta</a>
        <p>Si tu no creaste esta cuenta, puedes ignorar el mensaje.</p>
        <p>${process.env.FRONTEND_URL}/confirmar/${token}</p>
        `
    });
};

export const emailOlvidePassword = async (datos) => {

    const { email, nombre, token } = datos;

    const transport = nodemailer.createTransport({
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS,
        }
    });

    // Información del email
    const info = await transport.sendMail({
        from: '"Tweeter - Descubre lo que esta pasando" <cuentas@tweeter.com>',
        to: email,
        subject: 'Tweeter - Restablece tu password',
        text: 'Restablece tu password',
        html: `
        <p>Hola: ${nombre} has solicitado reestablecer tu password.</p>
        <p>Sigue el siguiente enlace para generar un nuevo password:</p>
        <a href="${process.env.FRONTEND_URL}/olvide-password/${token}">Reestablecer Password</a>
        <p>Si tu no solicitaste esta email, puedes ignorar el mensaje.</p>
        `
    });
};