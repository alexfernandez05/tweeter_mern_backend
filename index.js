import express from "express";
import dotenv from "dotenv";
import cors from "cors";

import conectarDB from "./config/db.js";

import usuarioRoutes from "./routes/usuarioRoutes.js"
import tweetRoutes from "./routes/tweetRoutes.js"

const app = express();
app.use(express.json());

dotenv.config();

conectarDB();

// Configurar CORS
const whitelist = [process.env.FRONTEND_URL];

const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.includes(origin)) {
            // Puede consultar la API
            callback(null, true);
        } else {
            // No esta permitido
            callback(new Error("ERROR de CORS"));
        }
    }
};

app.use(cors(corsOptions));

// Routing
app.use("/api/usuarios", usuarioRoutes)
app.use('/api/tweet', tweetRoutes);

const PORT = process.env.PORT || 4000;

const servidor = app.listen(PORT, () => {
    console.log(`Servidor corriendo en el puerto ${PORT}`);
});


// Socket.io
var clients = 0;

import { Server } from 'socket.io'

const io = new Server(servidor, {
    pingTimeout: 60000,
    cors: {
        origin: process.env.FRONTEND_URL,
    }
});

io.on('connection', (socket) => {
    clients++;
    io.sockets.emit('broadcast', { description: clients + ' clients connected!' });

    console.log('Conectado a socket.io');

    socket.on('nuevo-tweet', (tweet) => {
        io.sockets.emit('tweet-anadido', tweet);
    })

    socket.on('nuevo-retweet', (tweet) => {
        io.sockets.emit('tweet-retweetado', tweet);
    })

    socket.on('nuevo-like', (tweet) => {
        io.sockets.emit('tweet-like', tweet);
    })

    // socket.on('nuevo-guardado', (tweet) => {
    //     io.sockets.emit('tweet-guardado', tweet);
    // })

    socket.on('nueva-respuesta', (tweet) => {
        console.log('nueva-respuesta');
        io.sockets.emit('respuesta-nueva', tweet);
    })
});