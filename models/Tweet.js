import mongoose from "mongoose";
import bcrypt from "bcrypt";

const tweetSchema = mongoose.Schema({
        esRespuesta: {
            type: Boolean,
            default: false,
        },
        esRetweet: {
            type: Object,
            default: null,
        },
        usuario: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Usuario',
        },
        fechaPublicacion: {
            type: Date,
            default: Date.now(),
            required: true,
        },
        contenido: {
            type: String,
            trim: true,
            requiered: false,
        },
        imagen: {
            type: String,
            required: false,
        },
        comentarios: {
            type: Number,
            default: 0,
            required: true,
        },
        retweets: [
            {
                type: Object
            }
        ],
        likes: [
            {
                type: Object
            }
        ],
        guardados: [
            {
                type: Object
            }
        ],
        listaComentarios: [
            {
                type: Object
            }
        ],
    },
    {
        timestamps: true,
    }
);

const Tweet = mongoose.model('Tweet', tweetSchema);
export default Tweet;