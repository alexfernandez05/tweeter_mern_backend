import express from 'express';
import {
    obtenerTweets,
    obtenerTweet,
    nuevoTweet,
    editarTweet
} from '../controllers/tweetController.js';

import checkAuth from '../middleware/checkAuth.js';

const router = express.Router();

router.route('/').get(checkAuth, obtenerTweets);
router.route('/:id').get(checkAuth, obtenerTweet);
router.route('/add').post(checkAuth, nuevoTweet);
router.route('/edit/:id').put(checkAuth, editarTweet);

export default router;