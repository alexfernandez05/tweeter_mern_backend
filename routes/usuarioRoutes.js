import express from "express";

const router = express.Router();

import {
    registrar,
    autenticar,
    confirmar,
    olvidePassword,
    comprobarToken,
    nuevoPassword,
    perfil,
    editarUsuario,
} from "../controllers/usuarioController.js"

import checkAuth from '../middleware/checkAuth.js';


// AUTENTICACIÓN, REGISTRO Y CONFIRMACIÓN DE USUARIOS
router.post('/', registrar);
router.post('/login', autenticar);
router.get('/confirmar/:token', confirmar)
router.post('/olvide-password', olvidePassword)
router.route("/olvide-password/:token")
    .get(comprobarToken)
    .post(nuevoPassword);

router.get('/perfil', checkAuth, perfil);
router.route('/edit/:id').put(checkAuth, editarUsuario);

export default router;